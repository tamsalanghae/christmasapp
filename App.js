import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, Button, TextInput } from "react-native";
import { useState, useEffect } from "react";
export default function App() {
  const [stateOdd, setStateOdd] = useState(false);
  const [stateEven, setStateEven] = useState(true);
  const [on, setOn] = useState(false);
  const [off, setOff] = useState(true);

  if (on == true) {
    setTimeout(() => {
      setStateEven(!stateEven);
      setStateOdd(!stateOdd);
    }, 1000);
  }
  console.log("odd: ", stateOdd);
  console.log("even: ", stateEven);

  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <View
          style={[
            styles.circle,
            styles.red,
            stateOdd ? { shadowRadius: 15 } : { shadowRadius: 0 },
            off ? { opacity: 0.2 } : null,
          ]}
        ></View>
        <View
          style={[
            styles.circle,
            styles.yellow,
            stateEven ? { shadowRadius: 15 } : { shadowRadius: 0 },
            off ? { opacity: 0.2 } : null,
          ]}
        ></View>
        <View
          style={[
            styles.circle,
            styles.blue,
            stateOdd ? { shadowRadius: 15 } : { shadowRadius: 0 },
            off ? { opacity: 0.2 } : null,
          ]}
        ></View>
        <View
          style={[
            styles.circle,
            styles.green,
            stateEven ? { shadowRadius: 15 } : { shadowRadius: 0 },
            off ? { opacity: 0.2 } : null,
          ]}
        ></View>
      </View>
      <View style={styles.utilities}>
        <View style={styles.title}>
          <Text style={styles.heading}>Christmas Lights</Text>
        </View>
        <View style={styles.buttons}>
          <Button
            style={{}}
            color="black"
            title="On"
            onPress={() => {
              setOn(true);
              setOff(false);
            }}
          />
          <Button
            style={{}}
            color="black"
            title="Off"
            onPress={() => {
              setOn(false);
              setStateOdd(false);
              setStateEven(false);
              setOff(true);
            }}
          />
          {/* <TextInput
            style={styles.input}
            // onChangeText={onChangeNumber}
            // value={number}
            placeholder=""
            keyboardType="numeric"
          />
          <Button
            style={{
              height: 100,
              width: 100,
              borderWidth: 1,
              borderColor: "white",
            }}
            color="white"
            title="Run"
          /> */}
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    position: "relative",
  },
  main: {
    // flex: 1,
    marginTop: 150,
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  heading: {
    marginTop: 80,
    color: "black",
    fontSize: 40,
    shadowColor: "#fff",
    shadowOffset: { width: 5, height: 5 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
  },
  circle: {
    marginTop: 50,
    margin: 10,
    borderRadius: 50,
    height: 50,
    width: 50,
  },
  red: {
    shadowColor: "red",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 2,
    backgroundColor: "red",
  },
  yellow: {
    backgroundColor: "yellow",
    shadowColor: "yellow",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 5,
  },
  blue: {
    backgroundColor: "blue",
    shadowColor: "blue",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 5,
  },
  green: {
    backgroundColor: "green",
    shadowColor: "green",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 5,
  },
  utilities: { flex: 2 },
  title: {},
  buttons: {
    marginTop: 80,
    flexDirection: "row",
    alignItems: "center",
    display: "flex",
    justifyContent: 'space-evenly',
    padding: 20,
    // backgroundColor: "pink",
  },
  input: {
    height: 50,
    width: 150,
    margin: 12,
    color: "white",
    borderWidth: 1,
    borderColor: "white",
    borderBottomLeftRadius: 10,
    borderTopLeftRadius: 10,
    padding: 10,
  },
});